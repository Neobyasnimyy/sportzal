<?php

namespace app\controllers;

use app\models\District;
use app\models\Gym;
use app\models\Service;
use app\models\Tag;
use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use yii\helpers\Url;
use app\models\RegisterForm;
use app\models\User;
use yii\web\NotFoundHttpException;


class SiteController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    public function actionGetGyms()
    {
        $gyms = Gym::find()->with(['gymServices','gymTags'])->asArray()->all();
        $services = Service::find()->asArray()->all();
        $tags = Tag::find()->asArray()->all();
        $districts= District::find()->asArray()->all();
        $streets = Gym::find()->select('street')->groupBy('street')->asArray()->all();
//        var_dump($streets);
//        die();
        return json_encode(['gyms'=>$gyms,'services'=>$services,'tags'=>$tags,'districts'=>$districts, 'streets'=>$streets]);
    }

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->redirect('admin/gym');
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->redirect('/admin/gym');
        }
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Register action
     *
     * @return string|Response
     */
    public function actionRegister()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }
        $model = new RegisterForm();
        if ($model->load(\Yii::$app->request->post()) && $model->validate()) {
            $user = new User();
            $user->username = $model->username;
            $user->email = $model->email;
            $user->role = 'user';
            $user->password = \Yii::$app->security->generatePasswordHash($model->password);
            if ($user->save()) {
                Yii::$app->session->setFlash('success', 'Регистрация прошла успешно'); // созданние одноразовых сообщений для пользователя(хранятся в сессии)
                return Yii::$app->response->redirect('login');
            }
        }
        return $this->render('register', compact('model'));
    }

    public function actionGym($id){

        if (($model = Gym::findOne($id)
            ) !== null) {
            return $this->render('gym', ['model'=>$model]);
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }

    }


}
