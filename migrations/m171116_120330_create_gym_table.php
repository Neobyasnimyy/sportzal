<?php

use yii\db\Migration;

/**
 * Handles the creation of table `gym`.
 */
class m171116_120330_create_gym_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('gym', [
            'id' => $this->primaryKey(),
            'gym_name'=>$this->string(255)->notNull(),
            'description'=>$this->text(),
            'price'=>$this->integer(),
            'district_id'=>$this->integer(255)->notNull(),
            'street'=>$this->string(255),
            'number'=>$this->string(255),
            'housing'=>$this->string(255),
            'start_time'=>$this->time(),
            'end_time'=>$this->time(),
        ],'CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci ENGINE=InnoDB');


        // creates index for column `district_id`
        $this->createIndex(
            'idx-gym-district_id',
            'gym',
            'district_id'
        );

        // add foreign key for table `district`
        $this->addForeignKey(
            'fk-gym-district_id',
            'gym',
            'district_id',
            'district',
            'id',
            'RESTRICT',
            'RESTRICT'
        );

    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        // drops foreign key for table `district`
        $this->dropForeignKey(
            'fk-gym-district_id',
            'gym'
        );

        // drops index for column `district_id`
        $this->dropIndex(
            'idx-gym-district_id',
            'gym'
        );



        $this->dropTable('gym');


    }
}
