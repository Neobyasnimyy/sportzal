<?php

use yii\db\Migration;

/**
 * Handles the creation of table `gym_services`.
 */
class m171116_123117_create_gym_services_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('gym_services', [
            'id' => $this->primaryKey(),
            'gym_id'=>$this->integer()->notNull(),
            'service_id'=>$this->integer()->notNull(),
        ],'CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci ENGINE=InnoDB');

        // creates index for column `gym_id`
        $this->createIndex(
            'idx-gym_services-gym_id',
            'gym_services',
            'gym_id'
        );

        // add foreign key for table `gym`
        $this->addForeignKey(
            'fk-gym_services-gym_id',
            'gym_services',
            'gym_id',
            'gym',
            'id',
            'CASCADE',
            'CASCADE'
        );



        // creates index for column `service_id`
        $this->createIndex(
            'idx-gym_services-service_id',
            'gym_services',
            'service_id'
        );

        // add foreign key for table `services`
        $this->addForeignKey(
            'fk-gym_services-service_id',
            'gym_services',
            'service_id',
            'services',
            'id',
            'CASCADE',
            'CASCADE'
        );

    }

    /**
     * @inheritdoc
     */
    public function down()
    {

        // drops foreign key for table `services`
        $this->dropForeignKey(
            'fk-gym_services-service_id',
            'gym_services'
        );

        // drops index for column `service_id`
        $this->dropIndex(
            'idx-gym_services-service_id',
            'gym_services'
        );



        // drops foreign key for table `gym`
        $this->dropForeignKey(
            'fk-gym_services-gym_id',
            'gym_services'
        );

        // drops index for column `gym_id`
        $this->dropIndex(
            'idx-gym_services-gym_id',
            'gym_service'
        );

        $this->dropTable('gym_services');
    }
}
