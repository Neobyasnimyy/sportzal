<?php

use yii\db\Migration;

/**
 * Handles the creation of table `user`.
 */
class m171116_113156_create_user_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('user', [
            'id' => $this->primaryKey(),
            'username'=>$this->string(),
            'email'=>$this->string(),
            'password'=>$this->string(),
            'auth_key'=>$this->string()->null(),
            'role'=>$this->string(),
        ],'CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci ENGINE=InnoDB');
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('user');
    }
}
