<?php

use yii\db\Migration;

/**
 * Handles the creation of table `gym_tags`.
 */
class m171116_124536_create_gym_tags_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('gym_tags', [
            'id' => $this->primaryKey(),
            'gym_id'=>$this->integer()->notNull(),
            'tag_id'=>$this->integer()->notNull(),
        ],'CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci ENGINE=InnoDB');

        // creates index for column `gym_id`
        $this->createIndex(
            'idx-gym_tags-gym_id',
            'gym_tags',
            'gym_id'
        );

        // add foreign key for table `gym`
        $this->addForeignKey(
            'fk-gym_tags-gym_id',
            'gym_tags',
            'gym_id',
            'gym',
            'id',
            'CASCADE',
            'CASCADE'
        );

        // creates index for column `tag_id`
        $this->createIndex(
            'idx-gym_tags-tag_id',
            'gym_tags',
            'tag_id'
        );

        // add foreign key for table `tags`
        $this->addForeignKey(
            'fk-gym_tags-tag_id',
            'gym_tags',
            'tag_id',
            'tags',
            'id',
            'CASCADE',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {

        // drops foreign key for table `tags`
        $this->dropForeignKey(
            'fk-gym_tags-tag_id',
            'gym_tags'
        );

        // drops index for column `tag_id`
        $this->dropIndex(
            'idx-gym_tags-tag_id',
            'gym_tags'
        );


        // drops foreign key for table `gym`
        $this->dropForeignKey(
            'fk-gym_tags-gym_id',
            'gym_tags'
        );

        // drops index for column `gym_id`
        $this->dropIndex(
            'idx-gym_tags-gym_id',
            'gym_tags'
        );

        $this->dropTable('gym_tags');
    }
}
