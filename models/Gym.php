<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "gym".
 *
 * @property integer $id
 * @property string $gym_name
 * @property string $description
 * @property integer $price
 * @property integer $district_id
 * @property string $street
 * @property string $number
 * @property string $housing
 * @property string $start_time
 * @property string $end_time
 *
 * @property District $district
 * @property GymServices[] $gymServices
 * @property GymTags[] $gymTags
 *
 */
class Gym extends ActiveRecord
{
    private $tags;
    private $services;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'gym';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['gym_name', 'district_id', 'start_time', 'end_time'], 'required'],
            [['description'], 'string'],
            [['gym_name', 'description', 'street', 'housing'], 'trim'],
            [['price', 'district_id', 'number'], 'integer'],
            [['start_time', 'end_time'], 'date', 'format' => 'php:H:i'],
            ['end_time', 'validateEndTime'],
            [['gym_name', 'street'], 'string', 'max' => 255],
            [['housing'], 'string', 'max' => 5],
            [['tags', 'services'], 'safe'],
//            ['tags', 'each', 'rule' => ['integer']],
            ['tags', 'validateTags'],
            ['services', 'validateServices'],
            [['district_id'], 'exist', 'skipOnError' => true, 'targetClass' => District::className(), 'targetAttribute' => ['district_id' => 'id']],
        ];
    }

    // наша созданная валидация, она будет работать на сервере уже
    public function validateTags($attribute)
    {
        $tag_list = self::getTagList();
        $tags = $this->tags;
        if (!empty($tags)) {
            foreach ($tags as $tag) {
                if (!array_key_exists($tag, $tag_list)) {
                    $this->addError($attribute, 'Такого направления не существует.');
                    break;
                }
            }
        }

    }

    public function validateServices($attribute)
    {
        $attribute;
        $service_list = self::getServiceList();
        $services = $this->services;
        if (!empty($services)) {
            foreach ($services as $service) {
                if (!array_key_exists($service, $service_list)) {
                    $this->addError($attribute, 'Такого сервиса не существует.');
                    break;
                }
            }
        }

    }

    public function validateEndTime($attribute)
    {
        $start = strtotime(date('Y-m-d')  ." ". $this->start_time);
        $end = strtotime(date('Y-m-d')  ." ". $this->end_time);
        if ($start > $end){
            $this->addError($attribute, 'Конечное время не может быть раньше начального.');
        }
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'gym_name' => 'Название зала или секции',
            'description' => 'Описание',
            'price' => 'Цена(грн)',
            'district_id' => 'Район',
            'street' => 'Улица',
            'number' => 'Номер дома',
            'housing' => 'корпус',
            'start_time' => 'с',
            'end_time' => 'по',
            'tags' => 'Направление',
            'services' => 'Сервис',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDistrict()
    {
        return $this->hasOne(District::className(), ['id' => 'district_id']);
    }

    /**
     * @return array District.name
     */
    public static function getDistrictList()
    {
        $districts = District::find()->select(['id', 'district_name'])->all();
        return ArrayHelper::map($districts, 'id', 'district_name');
    }

    public function getDistrictName()
    {
        $list= self::getDistrictList();
        return $list[$this->district_id];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGymTags()
    {
        return $this->hasMany(GymTags::className(), ['gym_id' => 'id']);
    }

    public function getTags()
    {
        $tags = [];
        foreach ($this->getGymTags()->all() as $item) {
            $tags[] = $item['tag_id'];
        }
        return $tags;
    }

    public function setTags($tags)
    {
        $this->tags = $tags;
    }

    /**
     * @return array Tag.name
     */
    public static function getTagList()
    {
        $tags = Tag::find()->select(['id', 'tag_name'])->all();
        return ArrayHelper::map($tags, 'id', 'tag_name');
    }

    // обновляем запись связанной таблицы gym_tags
    public function updateGymTags()
    {
        $tags = $this->tags;
        $gymTags = $this->gymTags;

        foreach ($gymTags as $item) {
            // если выбранный tag раньше был отмечен
            if (!empty($tags) && in_array($item->tag_id, $tags)) {
                unset($tags[array_search($item->tag_id, $tags)]);
            } else {
                // если tag убрали удаляем связь
                $item->delete();
            }
        }
        if (!empty($tags) && is_array($tags)) {
            foreach ($tags as $tag) {
                $gim_tags = new GymTags();
                $gim_tags->gym_id = $this->id;
                $gim_tags->tag_id = $tag;
                if ($gim_tags->validate()) {
                    $gim_tags->save();
                }
            }
        }
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGymServices()
    {
        return $this->hasMany(GymServices::className(), ['gym_id' => 'id']);
    }

    public function getServices()
    {
        $services = [];
        foreach ($this->getGymServices()->all() as $item) {
            $services[] = $item['service_id'];
        }
        return $services;
    }

    /**
     * @param $services
     */
    public function setServices($services)
    {
        $this->services = $services;
    }

    /**
     * @return array Service.name
     */
    public static function getServiceList()
    {
        $services = Service::find()->select(['id', 'service_name'])->all();
        return ArrayHelper::map($services, 'id', 'service_name');
    }

    // обновляем запись связанной таблицы gym_services
    public function updateGymServices()
    {
        $services = $this->services;
        $gymServices = $this->gymServices;

        foreach ($gymServices as $item) {
            // если выбранный service раньше был отмечен
            if (!empty($services) && in_array($item->service_id, $services)) {
                unset($services[array_search($item->service_id, $services)]);
            } else {
                // если service убрали, удаляем связь
                $item->delete();
            }
        }
        if (!empty($services) && is_array($services)) {
            foreach ($services as $service) {
                $gim_services = new GymServices();
                $gim_services->gym_id = $this->id;
                $gim_services->service_id = $service;
                if ($gim_services->validate()) {
                    $gim_services->save();
                }
            }
        }
    }

    public function afterSave($insert, $changedAttributes)
    {
        $this->updateGymTags();

        $this->updateGymServices();

        parent::afterSave($insert, $changedAttributes); // TODO: Change the autogenerated stub
    }
}
