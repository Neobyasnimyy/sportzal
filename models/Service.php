<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "services".
 *
 * @property integer $id
 * @property string $service_name
 *
 * @property GymServices[] $gymServices
 */
class Service extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'services';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['service_name'], 'required'],
            [['service_name'], 'trim'],
            [['service_name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'service_name' => 'Название сервиса',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGymServices()
    {
        return $this->hasMany(GymServices::className(), ['service_id' => 'id']);
    }
}
