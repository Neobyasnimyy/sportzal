<?php

namespace app\modules\admin\controllers;

use app\models\District;
use app\models\GymTags;
use app\models\Tag;
use Yii;
use app\models\Gym;
use app\modules\admin\models\GymSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * GymController implements the CRUD actions for Gym model.
 */
class GymController extends AdminController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Gym models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new GymSearch();
        $districtList = Gym::getDistrictList();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'districtList' => $districtList,
        ]);
    }


    /**
     * Creates a new Gym model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Gym();
        $districtList = Gym::getDistrictList();
        $tagList = Gym::getTagList();
        $serviceList = Gym::getServiceList();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index']);
        }

        return $this->render('create', [
            'model' => $model,
            'districtList' => $districtList,
            'tagList' => $tagList,
            'serviceList' => $serviceList,
        ]);

    }

    /**
     * Updates an existing Gym model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $districtList = Gym::getDistrictList();
        $tagList = Gym::getTagList();
        $serviceList = Gym::getServiceList();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {

            Yii::$app->session->setFlash('success', "Данные сохранены."); // созданние одноразовых сообщений для пользователя(хранятся в сессии)
            return $this->redirect(['index']);
        }

        return $this->render('update', [
            'model' => $model,
            'districtList' => $districtList,
            'tagList' => $tagList,
            'serviceList' => $serviceList,
        ]);

    }

    /**
     * Deletes an existing Gym model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Gym model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Gym the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Gym::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
