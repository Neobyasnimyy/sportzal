<?php


namespace app\modules\admin\controllers;

use Yii;
use app\modules\admin\models\DistrictSearch;
use app\models\District;
use app\models\Service;
use app\modules\admin\models\ServiceSearch;
use app\models\Tag;
use app\modules\admin\models\TagSearch;
use yii\filters\VerbFilter;


class SettingsController extends AdminController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                    'update' => ['POST'],
                ],
            ],
        ];
    }

    public function actionIndex()
    {
        $modelDistrict = new District();
        $searchModelDistrict = new DistrictSearch();
        $dataProviderDistrict = $searchModelDistrict->search(Yii::$app->request->queryParams);

        if ($modelDistrict->load(Yii::$app->request->post()) && $modelDistrict->save()) {
            Yii::$app->session->setFlash('success', 'Данные сохранены.');
            return $this->redirect(['index']);
        }

        $modelService = new Service();
        $searchModelService = new ServiceSearch();
        $dataProviderService = $searchModelService->search(Yii::$app->request->queryParams);

        if ($modelService->load(Yii::$app->request->post()) && $modelService->save()) {
            Yii::$app->session->setFlash('success', 'Данные сохранены.');
            return $this->redirect(['index']);
        }

        $modelTag = new Tag();
        $searchModelTag = new TagSearch();
        $dataProviderTag = $searchModelTag->search(Yii::$app->request->queryParams);

        if ($modelTag->load(Yii::$app->request->post()) && $modelTag->save()) {
            Yii::$app->session->setFlash('success', 'Данные сохранены.');
            return $this->redirect(['index']);
        }


        return $this->render('index', [
            'modelDistrict' => $modelDistrict,
            'searchModelDistrict' => $searchModelDistrict,
            'dataProviderDistrict' => $dataProviderDistrict,

            'modelService' => $modelService,
            'searchModelService' => $searchModelService,
            'dataProviderService' => $dataProviderService,

            'modelTag' => $modelTag,
            'searchModelTag' => $searchModelTag,
            'dataProviderTag' => $dataProviderTag,
        ]);
    }

    public function actionDelete($modelName, $id)
    {
        $modelName = 'app\models\\' . $modelName;
        if (($model = $modelName::findOne($id)) !== null) {
            $model->delete();
            Yii::$app->session->setFlash('success', 'Данные успешно удалены.');
        }
        return $this->redirect(['index']);
    }

    public function actionUpdate($modelName,$id)
    {
//        var_dump($modelName);
//        var_dump($_POST);
//        die();
        if (Yii::$app->request->isAjax ){
            $urlModel = 'app\models\\' . $modelName;
            if ((($model = $urlModel::findOne($id)) !== null) && ($model->load(Yii::$app->request->post()))) {
                $model->save();
                return $this->renderPartial('_form-update-name',[
                    'model'=>$model,
                    'modelName'=>$modelName
                ]);
            }
        }

        return $this->redirect('index');
    }
}