<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

?>


<div class="_div-form-update-name">

    <?php $form = ActiveForm::begin([
        'id' => "_update-{$modelName}-{$model->id}",
//            'action' => ['update-category'],
        'enableClientValidation' => true, // проверка на стороне клиента полностью,
//        'method' => 'post',
        'options' => [
            'class'=>'_updateForm',
            'data-model-name'=>$modelName,
            'data-id'=>$model->id,
//            'data-pjax' => 0,
        ],
    ]); ?>


    <?php $modelName= mb_strtolower($modelName);
     echo $form->field($model, "{$modelName}_name")
        ->label(false)
        ->textInput(['style'=>'background-color: #eee;cursor:pointer;','title'=>'изменить']); ?>

<!--    --><?php //echo $form->field($model, 'id')->label(false)->hiddenInput(); ?>


    <?php ActiveForm::end(); ?>

</div>
