<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\helpers\Url;

$this->registerJsFile('/admin/js/settings/index.js',
    ['depends' => [\yii\web\JqueryAsset::className()]]);

$this->title = 'Настройки';
$this->params['breadcrumbs'][] = ['label' => 'Залы', 'url' => ['/admin/gym']];
$this->params['breadcrumbs'][] = ['label' => 'Настройки', 'url' => ['/admin/settings']];
?>
<div class="settings-index row">

    <h2 class="text-center"><?= Html::encode($this->title) ?></h2>

    <div class="district col-md-4">

        <?php echo $this->render('_form-district', [
            'model' => $modelDistrict,
        ]) ?>


        <?php Pjax::begin([
            'id'=>'district-pjax'
        ]); ?>


        <?php echo GridView::widget([
            'dataProvider' => $dataProviderDistrict,
            'filterModel' => $searchModelDistrict,
            'layout' => "{items}\n{pager}",
            'columns' => [
//            ['class' => 'yii\grid\SerialColumn'],

                [
                    'attribute' => 'id',
                    'headerOptions' => ['width' => '60'],
                ],
                [
                    'attribute' => 'district_name',
                    'format' => 'raw',
                    'value' => function ($data) {
                        return $this->render('_form-update-name', [
                            'model' => $data,
                            'modelName'=>'District'
                        ]);
                    },
                ],


//            ['class' => 'yii\grid\ActionColumn'],
            ],
        ]); ?>

        <?php Pjax::end(); ?>

    </div>

    <div class="service col-md-4">

        <?php echo $this->render('_form-service', [
            'model' => $modelService,
        ]) ?>


        <?php Pjax::begin([
            'id'=>'service-pjax'
        ]); ?>


        <?php echo GridView::widget([
            'dataProvider' => $dataProviderService,
            'filterModel' => $searchModelService,
            'layout' => "{items}\n{pager}",
            'columns' => [
//            ['class' => 'yii\grid\SerialColumn'],
                [
                    'attribute' => 'id',
                    'headerOptions' => ['width' => '60'],
                ],
                [
                    'attribute' => 'service_name',
                    'format' => 'raw',
                    'value' => function ($data) {
                        return $this->render('_form-update-name', [
                            'model' => $data,
                            'modelName'=>'Service'
                        ]);
                    },
                ],
                ['class' => 'yii\grid\ActionColumn',
                    'template' => '{delete}',
                    'urlCreator' => function ($action, $model, $key, $index) {
                        return Url::to(['/admin/settings/' . $action , 'modelName'=>'Service','id' => $model->id]);
                    }
                ],            ],
        ]); ?>

        <?php Pjax::end(); ?>

    </div>

    <div class="tag col-md-4">

        <?= $this->render('_form-tag', [
            'model' => $modelTag,
        ]) ?>


        <?php Pjax::begin([
            'id'=>'tag-pjax'
        ]); ?>


        <?php echo GridView::widget([
            'dataProvider' => $dataProviderTag,
            'filterModel' => $searchModelTag,
            'layout' => "{items}\n{pager}",
            'columns' => [
//            ['class' => 'yii\grid\SerialColumn'],
                [
                    'attribute' => 'id',
                    'headerOptions' => ['width' => '60'],
                ],
//                'tag_name',
                [
                    'attribute' => 'tag_name',
                    'format' => 'raw',
                    'value' => function ($data) {
                        return $this->render('_form-update-name', [
                            'model' => $data,
                            'modelName'=>'Tag'
                        ]);
                    },
                ],

                ['class' => 'yii\grid\ActionColumn',
                    'template' => '{delete}',
                    'urlCreator' => function ($action, $model, $key, $index) {
                        return Url::to(['/admin/settings/' . $action , 'modelName'=>'Tag','id' => $model->id]);

                    }
                ],
            ],
        ]); ?>

        <?php Pjax::end(); ?>

    </div>
</div>
