<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\time\TimePicker;
use Faker\Provider\DateTime;

/* @var $this yii\web\View */
/* @var $model app\models\Gym */
/* @var $form yii\widgets\ActiveForm */
//var_dump($model);
//var_dump($model->gymTags);
//var_dump($model->tags);
//die();
?>

<div class="gym-form">

    <?php $form = ActiveForm::begin(); ?>

    <?php echo $form->field($model, 'gym_name')->textInput(['maxlength' => true]) ?>

    <?php echo $form->field($model, 'description')->textarea(['rows' => 6]) ?>

    <?php echo $form->field($model, 'price')->textInput() ?>

    <?php echo $form->field($model, 'district_id')->dropDownList($districtList) ?>


    <?php echo $form->field($model, 'street')->textInput(['maxlength' => true]) ?>

    <?php echo $form->field($model, 'number')->textInput(['maxlength' => true]) ?>

    <?php echo $form->field($model, 'housing')->textInput(['maxlength' => true]) ?>
    <div class="tags">
<!--                --><?php //echo $form->field($model,'tags')->checkboxList($tags) ?>
        <?php echo $form->field($model, 'tags')->checkboxList($tagList) ?>

    </div>

    <div class="services">
        <?php echo $form->field($model, 'services')->checkboxList($serviceList) ?>
    </div>

    <?php echo $form->field($model, 'start_time')->widget(TimePicker::classname(), [
//        'size' => 'xs',
        'pluginOptions' => [
            'showSeconds' => false,
            'showMeridian' => false,
            'minuteStep' => 15,
            'defaultTime' => '08:00'
//        'defaultTime' => date('H:i', strtotime('-2 hour')),
        ]]); ?>

    <?php echo $form->field($model, 'end_time')->widget(TimePicker::classname(), [
//        'size' => 'xs',
        'pluginOptions' => [
            'showSeconds' => false,
            'showMeridian' => false,
            'minuteStep' => 15,
            'defaultTime' => '22:00'
//        'defaultTime' => date('H:i', strtotime('-2 hour')),
        ]]); ?>

    <div class="form-group">
        <?php echo Html::submitButton('Сохранить', ['class' => 'btn btn-success' ]) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>