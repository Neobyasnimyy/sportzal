<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\admin\models\GymSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Залы';
$this->params['breadcrumbs'][] = ['label' => $this->title, 'url' => ['/admin/gym']];
$this->params['breadcrumbs'][] = ['label' => 'Настройки', 'url' => ['/admin/settings']];


?>
<div class="gym-index">

    <h2 class="text-center"><?= Html::encode($this->title) ?></h2>
    <!--    --><?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p class="text-right">
        <?= Html::a('Создать', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?php Pjax::begin(); ?>    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'layout' => "{items}\n{pager}",
        'columns' => [
//            ['class' => 'yii\grid\SerialColumn'],
//            'id',
//            'gym_name',
            ['attribute' => 'gym_name',
                'format' => 'html',
                'value' => function ($data) {
                    return Html::a($data->gym_name, Url::toRoute(['gym/update', 'id' => $data->id]), ['title' => 'Редактировать']);
                },
            ],
//            'description:ntext',
            'price',
            [
                'attribute' => 'district_id',
                'format' => 'raw',

                'value' => function ($data) {
                    return $data->getDistrictName();
                },
                'filter' =>$districtList,
            ],
            // 'street',
            // 'number',
            // 'housing',
            // 'start_time',
            // 'end_time',

            ['class' => 'yii\grid\ActionColumn',
//                'template' => '{update}{delete}',
                'urlCreator' => function ($action, $model, $key, $index) {
                    if ($action == 'view') {
                        return Url::to(['/gym/' . $action, 'id' => $model->id]);
                    }else{
                        return Url::to(['/admin/gym/' . $action, 'id' => $model->id]);
                    }
                }
            ],
        ],
    ]); ?>
<?php Pjax::end(); ?></div>
