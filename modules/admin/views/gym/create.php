<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Gym */

$this->title = 'Новый зал';
$this->params['breadcrumbs'][] = ['label' => 'Залы', 'url' => ['/admin/gym']];
$this->params['breadcrumbs'][] = ['label' => 'Настройки', 'url' => ['/admin/settings']];
?>
<div class="gym-create">

    <h2><?= Html::encode($this->title) ?></h2>

    <?= $this->render('_form', [
        'model' => $model,
        'districtList' => $districtList,
        'tagList'=>$tagList,
        'serviceList'=>$serviceList,
        ]) ?>

</div>
