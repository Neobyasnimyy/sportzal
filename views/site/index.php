<?php

/* @var $this yii\web\View */


$this->title = 'SportZal';
$this->registerCssFile('angular-moment-picker\angular-moment-picker.css');
$this->registerJsFile('js\angular.min.js', ['position' => yii\web\View::POS_HEAD]);
$this->registerJsFile('angular-moment-picker\angular-moment-picker.js', ['position' => yii\web\View::POS_HEAD]);

?>
<div class="site-index"  >

    <div ng-app="myApp" ng-controller="getGyms">
        <label for="search_gym_name"> Название: </label><br>
        <input class="form-control" type="text" id="search_gym_name" ng-model="search.gym_name">

        <label for="search_gym_district"> Район: </label>
        <select class="form-control" id="search_gym_district" ng-model="search.district_id">
            <option ng-repeat="x in districts" value="{{x.id}}">{{x.district_name}}</option>
        </select>

        <label for="search_gym_street"> Улица: </label><br>
        <input class="form-control" list="street" type="text" id="search_gym_street" ng-model="search.street">
        <datalist id="street">
            <option ng-repeat="x in streets" value="{{x.street}}">{{x.street}}</option>
        </datalist>
        <b>Направления:</b>
        <div>
            <div ng-repeat="x in tags">
                <label for="search_gym_tags_{{x.id}}">
                    <input id="search_gym_tags_{{x.id}}" ng-click="writeChecked( x.id,'tag' )"
                           type="checkbox">
                    {{x.tag_name}}
                </label>
            </div>
        </div>
        <hr>
        <b>Сервисы:</b>
        <div>
            <div ng-repeat="x in services">
                <label for="search_gym_services_{{x.id}}">
                    <input id="search_gym_services_{{x.id}}" ng-click="writeChecked( x.id,'service' )"
                           type="checkbox">
                    {{x.service_name}}
                </label>
            </div>
        </div>

        <label for="search_gym_price"> Цена: </label><br>
        <input class="form-control" type="text" id="search_gym_price" ng-model="search.price"
               onkeypress="return isNumberKey(event)">
        <br>

        <label>Включить фильтрацию по времени:
            <input type="checkbox" ng-model="checked_filter_time">
        </label><br/>

        <div ng-if="checked_filter_time">
            Открыто с:
            <div class="input-group"
                 locale="ru"
                 set-on-select="false"
                 show-header="false"
                 moment-picker="search_start_time"
                 format="HH:mm">
            <span class="input-group-addon">
                <i class="glyphicon glyphicon-time"></i>
            </span>
                <input class="form-control"
                       placeholder="Select a time"
                       ng-model="search_start_time"
                       ng-blur="writeSearchStartTime(search_start_time)"
                >
            </div>

            Закрывается в:
            <div class="input-group"

                 locale="ru"
                 set-on-select="false"
                 show-header="false"
                 moment-picker="search_end_time"
                 format="HH:mm">
            <span class="input-group-addon">
                <i class="glyphicon glyphicon-time"></i>
            </span>
                <input class="form-control"
                       placeholder="Select a time"
                       ng-model="search_end_time"
                       ng-blur="writeSearchEndTime(search_end_time)"
                >
            </div>

        </div>

        <p>Список залов:</p>
        <div class=" table-responsive" style="overflow: auto; height: 200px">
            <table class="table table-striped table-bordered">
                <thead>
                <tr>
                    <th>Название</th>
                    <th>Район</th>
                    <th>Улица</th>
                    <th>Направление</th>
                    <th>Сервисы</th>
                    <th>Цена,грн</th>
                    <th>время с</th>
                    <th>время по</th>
                </tr>
                </thead>
                <tbody>
                <tr ng-repeat="gym in gyms | filter:filteredGyms | filter:search">

                    <td><a href="/site/gym?id={{gym.id}}"> {{gym.gym_name}}</a></td>
                    <td>{{districts[gym.district_id-1].district_name}}</td>
                    <td>{{gym.street}}</td>

                    <td>
                        <span ng-repeat="serv in gym.gymServices">
                            {{services[serv.service_id -1].service_name}};
                        </span>
                    </td>

                    <td>
                        <span ng-repeat="item in gym.gymTags">
                            {{tags[item.tag_id -1].tag_name}};
                        </span>
                    </td>

                    <td>{{gym.price}}</td>
                    <td>{{gym.start_time | formatTime}}</td>
                    <td>{{gym.end_time | formatTime}}</td>
                </tr>
                </tbody>
            </table>


        </div>

        <script>
            var app = angular.module('myApp', ['moment-picker']);

            Array.prototype.hasAll = function (a) {
                var hash = this.reduce(function (acc, i) {
                    acc[i] = true;
                    return acc;
                }, {});
                return a.every(function (i) {
                    return i in hash;
                });
            };

            // фильтрация на ввод только цифры
            function isNumberKey(evt) {
                var charCode = (evt.which) ? evt.which : event.keyCode;
                if (charCode > 31 && (charCode < 48 || charCode > 57))
                    return false;
                return true;
            }


            app.filter('formatTime', function ($filter) {
                return function (time, format) {
                    var parts = time.split(':');
                    var date = new Date(0, 0, 0, parts[0], parts[1], parts[2]);
                    return $filter('date')(date, format || 'HH:mm');
                };
            });


            app.controller('getGyms', function ($scope, $http) {

                $scope.search_start_time = "10:00";
                $scope.search_end_time = "20:00";


                $scope.search_checked = {
                    'tag': [],
                    'service': []
                };

                $scope.checked_filter_time = false;

                $scope.writeSearchStartTime = function (search_start_time) {
                    $scope.search_start_time = search_start_time;
                };

                $scope.writeSearchEndTime = function (search_end_time) {
                    $scope.search_end_time = search_end_time;
                };

                $scope.writeChecked = function (colour, name) {
                    var i = $.inArray(colour, $scope.search_checked[name]);
                    if (i > -1) {
                        $scope.search_checked[name].splice(i, 1);
                    } else {
                        $scope.search_checked[name].push(colour);
                    }
                };


                $scope.filteredGyms = function (gym) {
                    var flag = true;
                    // почему то тут он проходит в два раза больше чем количество залов
                    console.log("test2");
                    if ($scope.search_checked['tag'].length > 0) {
                        var arr_tags = [];
                        //создаем массив тегов, которые есть в конкретном зале
                        // создаем массив tag_id из связанной таблицы gym.gymTags
                        gym.gymTags.map(function (item) {
                            arr_tags[arr_tags.length] = item.tag_id;
                        });

                        //проверяем вхождение всех выбранных тегов в в теги зала
                        if (arr_tags.hasAll($scope.search_checked['tag'])) {
                            flag = true;
                        } else {
                            //не возвращает залы, если в них нет всех выбранных тегов
                            flag = false;
                        }
                    }

                    if (($scope.search_checked['service'].length > 0) && (flag)) {
                        var arr_services = [];
                        //создаем массив тегов, которые есть в конкретном зале
                        // создаем массив tag_id из связанной таблицы gym.gymTags
                        gym.gymServices.map(function (item) {
                            arr_services[arr_services.length] = item.service_id;
                        });

                        //проверяем вхождение всех выбранных тегов в в теги зала
                        if (arr_services.hasAll($scope.search_checked['service'])) {
                            flag = true;
                        } else {
                            //не возвращает залы, если в них нет всех выбранных тегов
                            flag = false;
                        }
                    }

                    if ($scope.checked_filter_time && (($scope.search_start_time) && ($scope.search_end_time))) {
//                        console.log($scope.checked_filter_time);
                        // тут вообще 60 раз проходит
                        console.log("test");
                        var arr_gym_start_time = (gym.start_time).split(":");
                        var gym_start_time = new Date().setHours(arr_gym_start_time[0], arr_gym_start_time[1]);

                        var arr_gym_end_time = (gym.end_time).split(":");
                        var gym_end_time = new Date().setHours(arr_gym_end_time[0], arr_gym_end_time[1]);

                        var arr_search_start_time = ($scope.search_start_time).split(":");
                        var search_start_time = new Date().setHours(arr_search_start_time[0], arr_search_start_time[1]);

                        var arr_search_end_time = ($scope.search_end_time).split(":");
                        var search_end_time = new Date().setHours(arr_search_end_time[0], arr_search_end_time[1]);

                        // очень много раз проходит по этой ветке!!!
//                        console.log("test");
//                        console.log(gym.end_time,"  ",$scope.search_end_time);
//                        console.log(gym_end_time,"  ",search_end_time);
//                        console.log(gym_end_time>search_end_time);
                        if (
                            ((gym_start_time <= search_start_time))// || (gym_start_time === search_start_time))
                            &&
                            ((gym_end_time >= search_end_time))// || (gym_end_time === search_end_time))
                        ) {
                            flag = true
                        } else {
                            flag = false
                        }
                    }

                    if (flag === true) {
                        return gym;
                    } else {
                        return false;
                    }

                };


                $http({
                    method: "GET",
                    url: "/site/get-gyms"
                })
                    .then(function (response) {
                        $scope.gyms = response.data.gyms;
                        $scope.services = response.data.services;
                        $scope.tags = response.data.tags;
                        $scope.districts = response.data.districts;
                        $scope.streets = response.data.streets;
                    });

            });
        </script>

    </div>
