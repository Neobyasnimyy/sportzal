$(document).ready(function () {


    $('.grid-view').on('click', '._updateForm input', function (e) {
        $(this).css("background", "#fff");
        return false;
    });


    $('.grid-view').on('blur', '._updateForm input', function (e) {

        if (($(this).attr('value')) != ($(this).val())) {
            var div = $(this).parents('._div-form-update-name');
            console.log('click');
            var form = $(this).parents('form');
            var model_name = form.data('model-name');
            var model_id = form.data('id');
            var data = form.serialize();

            $.ajax({
                url: '/admin/settings/update?modelName=' + model_name + '&id=' + model_id,
                type: 'POST',
                data: data,
                success: function (res) {
                    div.html(res);
                },
                timeout: 3000, // установка 3-х секундного тайм-аута
                error: function () {
                    alert('Ошибка!');
                }
            });
        } else {
            $(this).css("background", "#eee");
        }
        return false;
    });


});